package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"strconv"
	"strings"
	"time"
)

func gcd(firstNumber int64, secondNumber int64) int64 {
	// Метод нахождения НОД (Алгоритм Евклида)
	if firstNumber%secondNumber == 0 {
		return secondNumber
	}
	if secondNumber%firstNumber == 0 {
		return firstNumber
	}
	if firstNumber > secondNumber {
		return gcd(firstNumber%secondNumber, secondNumber)
	}
	return gcd(firstNumber, secondNumber%firstNumber)
}

func checkBasisMutuallySimple(arr []int64) bool {
	// Метод для проверки оснований на взаимопростоту оснований
	for index, _ := range arr {
		if index == 0 {
			continue
		}
		if gcd(arr[index-1], arr[index]) != 1 {
			return false
		}
	}
	return true
}

func findMaxNumberInRange(arr []int64) int64 {
	var temp = int64(1)
	for _, val := range arr {
		temp *= val
	}
	return temp - 1
}

func divisionMethod(number int64, arr []int64) {
	// Метод делением
	fmt.Println("Преобразование числа в СОК методом деления")
	start := time.Now()
	runtime := time.Since(start)
	fmt.Println("Число в СОК", toRNS(number, arr))
	fmt.Println("Время выполнения, нс: ", runtime.Nanoseconds())
}

func toRNS(number int64, arr []int64) []int64 {
	sok := make([]int64, 0, len(arr))
	for _, val := range arr {
		sok = append(sok, number%val)
	}
	return sok
}

func sequentialMultiplicationAndSummations(number int64, arr []int64) {
	// Метод последовательного умножения и суммирования
	fmt.Println("Преобразование числа в СОК методом последовательного умножения и суммирования")
	start := time.Now()
	sok := make([]int64, 0, len(arr))
	//goland:noinspection SpellCheckingInspection
	rnsnum1 := make([][]int64, 0, len(arr))
	//goland:noinspection SpellCheckingInspection
	rnsnum2 := make([][]int64, 0, len(arr))
	st := 0
	oldVal := number
	for number != 0 {
		rnsnum1 = append(rnsnum1, toRNS(number%10, arr))
		rnsnum2 = append(rnsnum2, toRNS(int64(math.Pow(float64(10), float64(st))), arr))
		st = st + 1
		number = number / 10
	}

	// переводим число в список чисел
	// region
	dirtyNumber := strings.Split(strconv.Itoa(int(oldVal)), "")
	numbers := make([]int64, 0, len(dirtyNumber))
	for _, value := range dirtyNumber {
		intVal, _ := strconv.Atoi(value)
		numbers = append(numbers, int64(intVal))
	}
	// endregion

	for i := 0; i < len(arr); i++ {
		sum := int64(0)
		for j, _ := range numbers {
			sum = sum + rnsnum1[j][i]*rnsnum2[j][i]
		}
		sok = append(sok, sum%arr[i])
	}
	runtime := time.Since(start)
	fmt.Println("Число в СОК", sok)
	fmt.Println("Время выполнения, нс: ", runtime.Nanoseconds())
}

func summationModularValues(number int64, arr []int64) {
	fmt.Println("Преобразование числа в СОК методом непосредственного суммирования модульных значений разрядов позиционного числа")
	start := time.Now()
	sok := make([]int64, 0, len(arr))
	// переводим число в список чисел
	// region
	dirtyNumber := strings.Split(strconv.Itoa(int(number)), "")
	numbers := make([]int64, 0, len(dirtyNumber))
	for _, value := range dirtyNumber {
		intVal, _ := strconv.Atoi(value)
		numbers = append(numbers, int64(intVal))
	}
	// endregion
	for _, val := range arr {
		subResult := numbers[0] * 10
		for _, subVal := range numbers[1 : len(numbers)-1] {
			subResult = (subResult + subVal%val) * 10 % val
		}
		subResult = subResult + numbers[len(numbers)-1]%val
		sok = append(sok, subResult%val)
	}
	runtime := time.Since(start)
	fmt.Println("Число в СОК", sok)
	fmt.Println("Время выполнения, нс: ", runtime.Nanoseconds())
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println("Введите базисы через пробел: ")
	// считываем данные
	scanner.Scan()
	// считываем базисы с консоли
	dirtyBasis := strings.Split(scanner.Text(), " ")
	// определяем количество базисов
	basisCount := len(dirtyBasis)

	basis := make([]int64, 0, basisCount)
	for _, value := range dirtyBasis {
		intVal, _ := strconv.Atoi(value)
		basis = append(basis, int64(intVal))
	}

	// проверяем основания
	if !checkBasisMutuallySimple(basis) {
		panic("Числа не являются взаимопростыми !")
	}

	var maxNumberInRange = findMaxNumberInRange(basis)
	fmt.Println("Максимальное число: ", maxNumberInRange)

	fmt.Println("Введите число, которое нужно представить в СОК: ")
	// считываем данные
	scanner.Scan()
	var number, _ = strconv.Atoi(scanner.Text())
	if int64(number) > maxNumberInRange {
		fmt.Println("Число не должно превышать максимальное число в диапазоне!")
	}
	fmt.Println()
	fmt.Println("Lab1")
	divisionMethod(int64(number), basis)
	fmt.Println()
	fmt.Println("Lab2")
	summationModularValues(int64(number), basis)
	fmt.Println()
	fmt.Println("Lab3")
	sequentialMultiplicationAndSummations(int64(number), basis)
	fmt.Println()
}
